package com.vinci.cameraprocessor.cameraproecssor.classification;

import com.vinci.cameraprocessor.cameraproecssor.skeleton.Skeleton;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class MoveFileAction {
    private CommandType destination;
    private Skeleton skeleton;
    private String depthImagePath;
}
