package com.vinci.cameraprocessor.cameraproecssor.classification;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CommandType {
    STANDING("standing"),
    SITTING("sitting"),
    WALKING("walking"),
    IGNORE("ignore");

    private String action;

    private static Map<String, CommandType> map;
    static {
        map = Stream.of(values())
                .collect(Collectors.toMap(CommandType::getAction, Function.identity()));
    }

    public String getAction() {
        return action;
    }

    CommandType(String action) {
        this.action = action;
    }

    public static CommandType get(String action) {
        return map.getOrDefault(action, null);
    }
}
