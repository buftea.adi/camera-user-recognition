package com.vinci.cameraprocessor.cameraproecssor.classification;

import com.google.common.collect.Lists;
import com.vinci.cameraprocessor.cameraproecssor.processor.util.FilesUtils;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Skeleton;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.SkeletonFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.vinci.cameraprocessor.cameraproecssor.Constants.*;

@Slf4j
@Service
public class DatasetService {

    private static Map<CommandType, AtomicInteger> indexes;

    @PostConstruct
    public void setUpProcessedDirectory() {
        FilesUtils.createDirectory(DATASET_BASE_PATH);
        Arrays.asList(CommandType.values()).forEach(commandType ->
                FilesUtils.createDirectory(getDestinationBaseDir(commandType))
        );

        indexes = Stream.of(CommandType.values()).collect(Collectors.toMap(
                Function.identity(),
                commandType -> new AtomicInteger(getInitIndex(commandType))
        ));
    }

    private Integer getInitIndex(CommandType commandType) {
        List<String> fileNames = FilesUtils.getFileNames(getDestinationBaseDir(commandType));
        return fileNames.stream()
                .map(FilenameUtils::removeExtension)
                .distinct()
                .map(s -> { try {return Integer.valueOf(s);} catch (Exception ignored) {} return null; })
                .filter(Objects::nonNull)
                .max(Integer::compareTo)
                .map(AtomicInteger::new)
                .orElseGet(() -> new AtomicInteger(0))
                .addAndGet(1);
    }

    @Async
    @EventListener
    public void handleEvent(MoveFileAction event) {
        execute(event);
    }

    private void execute(MoveFileAction moveAction) {
        Integer index = indexes.get(moveAction.getDestination()).getAndAdd(1);
        String depthFilePath = getDepthPath(moveAction, index);
        String skeletonPath = getSkeletonPath(moveAction, index);
        Skeleton skeleton = moveAction.getSkeleton();
        skeleton.setIndex(0L);
        SkeletonFile skeletonFile = SkeletonFile.builder()
                .number(1L)
                .skeletons(Collections.singletonList(skeleton))
                .build();
        FilesUtils.saveToFile(skeletonFile, skeletonPath);
        FilesUtils.copyFile(moveAction.getDepthImagePath(), depthFilePath);
    }

    private String getSkeletonPath(MoveFileAction moveAction, Integer index) {
        return getDestinationDir(moveAction, index) + JSON_EXTENSION;
    }

    private String getDepthPath(MoveFileAction moveAction, Integer index) {
        return getDestinationDir(moveAction, index) + IMAGE_EXTENSION;
    }

    private String getDestinationDir(MoveFileAction moveAction, Integer index) {
        return getDestinationBaseDir(moveAction.getDestination()) + "/" + index ;
    }

    private String getDestinationBaseDir(CommandType commandType) {
        return DATASET_BASE_PATH + "/" + commandType.getAction() ;
    }

    @Scheduled(fixedDelay = Long.MAX_VALUE)
    public void moveInSetsOfSix() {
        List<String> dirs = Stream.of(CommandType.values())
                .map(CommandType::getAction)
                .map(type -> DATASET_BASE_PATH + "/" + type)
                .sorted()
                .collect(Collectors.toList());

        for (String dir : dirs) {
            List<String> files = FilesUtils.getFileNames(dir).stream()
                    .filter(FilesUtils::isNotDirectory)
                    .sorted().collect(Collectors.toList());

            List<List<String>> partitions = Lists.partition(files, 12);
            int partitionIndex = 1;
            for (List<String> partition : partitions) {
                String partitionDir = dir + "/" + partitionIndex;
                FilesUtils.createDirectory(partitionDir);
                List<List<String>> groups = Lists.partition(partition.stream().sorted().collect(Collectors.toList()), 2);
                int groupIndex = 0;
                for (List<String> group : groups) {
                    for (String file : group) {
                        FilesUtils.copyFile(dir + "/" + file,
                                partitionDir + "/" + groupIndex + "." + FilenameUtils.getExtension(file));
                    }
                    groupIndex++;
                }
                partitionIndex++;
            }
        }

    }
}
