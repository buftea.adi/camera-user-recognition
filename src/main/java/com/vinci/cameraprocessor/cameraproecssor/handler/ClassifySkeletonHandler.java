package com.vinci.cameraprocessor.cameraproecssor.handler;

import com.vinci.cameraprocessor.cameraproecssor.processor.ClassificationProcessor;
import com.vinci.cameraprocessor.cameraproecssor.processor.DrawingProcessor;
import com.vinci.cameraprocessor.cameraproecssor.processor.SkeletonProcessor;
import com.vinci.cameraprocessor.cameraproecssor.processor.util.FilesUtils;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.SkeletonFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import java.util.Optional;

import static com.vinci.cameraprocessor.cameraproecssor.Constants.PROCESSED_DIR_VALUE;

@Slf4j
@Service
public class ClassifySkeletonHandler extends AbstractHandler {

    @Autowired
    private SkeletonProcessor skeletonProcessor;

    @Autowired
    private ClassificationProcessor classificationProcessor;

    @PostConstruct
    public void setUpProcessedDirectories() {
        FilesUtils.createDirectory(PROCESSED_DIR_VALUE);
    }

    public void handle(String filePath) {
        Optional<SkeletonFile> skeletonFile = skeletonProcessor.readFile(filePath);
        skeletonFile.ifPresent(file -> classificationProcessor.classify(filePath, file));
    }

}
