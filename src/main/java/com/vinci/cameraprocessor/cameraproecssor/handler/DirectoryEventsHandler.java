package com.vinci.cameraprocessor.cameraproecssor.handler;

import com.vinci.cameraprocessor.cameraproecssor.gateway.CameraData;
import com.vinci.cameraprocessor.cameraproecssor.gateway.GatewayService;
import com.vinci.cameraprocessor.cameraproecssor.gateway.UsersService;
import com.vinci.cameraprocessor.cameraproecssor.processor.BraceletColorProcessor;
import com.vinci.cameraprocessor.cameraproecssor.processor.SkeletonProcessor;
import com.vinci.cameraprocessor.cameraproecssor.processor.util.FilesUtils;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Joint;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Skeleton;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.SkeletonFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static com.vinci.cameraprocessor.cameraproecssor.Constants.PROCESSED_DIR_VALUE;

@Slf4j
@Service
@Deprecated
public class DirectoryEventsHandler extends AbstractHandler {

    @Autowired
    private SkeletonProcessor skeletonProcessor;

    @Autowired
    private BraceletColorProcessor braceletColorProcessor;

    @Autowired
    private GatewayService gatewayService;

    @Autowired
    private UsersService usersService;

    @PostConstruct
    public void setUpProcessedDirectory() {
        FilesUtils.createDirectory(PROCESSED_DIR_VALUE);
    }

    public void handle(String filePath) {
        String timestamp = getTimestamp(filePath);
        String deviceId = getDeviceId(filePath);
        Optional<SkeletonFile> skeletonFile = skeletonProcessor.readFile(filePath);
        if (skeletonFile.isPresent() && skeletonProcessor.shouldProcess(skeletonFile.get())) {
            InputStream rgbInputStream = FilesUtils.getRgbInputStream(deviceId, timestamp);
            InputStream depthInputStream = FilesUtils.getDepthInputStream(timestamp);
            Optional<BufferedImage> rgbImage = FilesUtils.readImage(rgbInputStream);
            Optional<BufferedImage> depthImage = FilesUtils.readImage(depthInputStream);

            if (!rgbImage.isPresent()) {
                log.error("RGB image not present. Skipping for deviceId <{}> and timestamp <{}>", deviceId, timestamp);
                return;
            }
            skeletonFile.get().getSkeletons().forEach(skeleton -> identifyAndSend(
                    rgbImage.get(),
                    depthImage.orElse(null),
                    skeleton,
                    timestamp,
                    deviceId)
            );

            FilesUtils.closeInputStream(rgbInputStream);
            FilesUtils.closeInputStream(depthInputStream);
            FilesUtils.moveToProcessed(filePath);
        }
    }

    private void identifyAndSend(BufferedImage rgbImage, BufferedImage depthImage, Skeleton skeleton, String timestamp, String deviceId) {
        Joint braceletJoint = skeletonProcessor.getBraceletJoint(skeleton);
        Optional<String> braceletColor = braceletColorProcessor.findBraceletColor(rgbImage, braceletJoint);
        if (!braceletColor.isPresent()) {
            log.warn("Bracelet color not visible. Skipping 1 skeleton for timestamp{}", timestamp);
            return;
        }
        Optional<Long> userId = usersService.getUserIdByBraceletColorAndDeviceId(braceletColor.get(), deviceId);
        if (!userId.isPresent()) {
            log.error("Could not find userId for color <{}> at timestamp <{}>", braceletColor, timestamp);
        }

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        try {
            ImageIO.write(depthImage, "png", bao);
            gatewayService.sendData(CameraData.builder()
                    .userId(userId.orElse(null))
                    .deviceId(deviceId)
                    .depthImage(bao.toByteArray())
                    .skeleton(skeleton)
                    .build());
        } catch (IOException e) {
            log.error("Failed sending data to Gateway for deviceId <{}> and timestamp <{}>", deviceId, timestamp, e);
        }
    }

}
