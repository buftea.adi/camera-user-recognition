package com.vinci.cameraprocessor.cameraproecssor.handler;

import org.apache.commons.io.FilenameUtils;

public abstract class AbstractHandler {

    public static String getTimestamp(String filePath) {
        /*-skeleton/TEST_APP_CAMERA_2_1575024383969.json-*/
        /*skeleton/TEST_APP_CAMERA_1575024383969.json*/
        return FilenameUtils.removeExtension(filePath).split("_")[3];

    }

    public static String getDeviceId(String filePath) {
        return filePath.split("_")[3];
    }

}
