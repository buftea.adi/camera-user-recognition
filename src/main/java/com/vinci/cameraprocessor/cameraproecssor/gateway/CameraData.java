package com.vinci.cameraprocessor.cameraproecssor.gateway;

import com.vinci.cameraprocessor.cameraproecssor.skeleton.Skeleton;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CameraData {
    private Long userId; /*will be set after bracelet identification*/
    private String deviceId;
    private Skeleton skeleton;
    private byte[] depthImage;
}
