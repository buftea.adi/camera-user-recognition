package com.vinci.cameraprocessor.cameraproecssor.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class GatewayService {
    private static final String GATEWAY_ENDPOINT = "http://141.85.223.52";

    @Autowired
    private RestTemplate restTemplate;

    public void sendData(CameraData data) {
        log.info("Sending data to Gateway: {}", data);

        /*TODO: uncomment sending, check CameraData mapping is correct*/
//        try {
//            restTemplate.postForObject(GATEWAY_ENDPOINT, data, void.class);
//        } catch (RestClientException e) {
//            log.error("Sending data to IO Server ({}) failed. Data: {}", GATEWAY_ENDPOINT, data, e);
//        }

    }
}
