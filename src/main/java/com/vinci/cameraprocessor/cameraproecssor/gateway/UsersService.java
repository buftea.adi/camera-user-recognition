package com.vinci.cameraprocessor.cameraproecssor.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class UsersService {

    /*Hardcoded mapping for users and bracelets until integration with tablet */
    private static Map<String, Map<String, Long>> users;
    static {
        Map<String, Long> bracelets = new HashMap<>();
        bracelets.put("red", 1L);
        bracelets.put("green", 2L);
        bracelets.put("blue", 3L);
        users = new HashMap<>();
        users.put("deviceId", bracelets);
    }

    public Optional<Long> getUserIdByBraceletColorAndDeviceId(String braceletColor, String deviceId) {
        log.info("Bracelet color identified: <{}>", braceletColor);
        /*TODO*/
        deviceId = "deviceId";
        return Optional.ofNullable(users.get(deviceId).get(braceletColor));
    }
}
