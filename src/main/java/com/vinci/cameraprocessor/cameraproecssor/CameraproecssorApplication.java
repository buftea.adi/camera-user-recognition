package com.vinci.cameraprocessor.cameraproecssor;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@SpringBootApplication
public class CameraproecssorApplication {

    public static void main(String[] args) {
//        SpringApplication.run(CameraproecssorApplication.class, args);
        /*Enables JFrame*/
        ConfigurableApplicationContext context = new SpringApplicationBuilder(CameraproecssorApplication.class)
                .headless(false)
                .run(args);
    }



    /*TODO
    *   verifica daca ai primit jsoane
    *   in json mai adauga si skeleton id
    *   in json, daca a aparut sau a disparut un schelet, fa (din nou) clasificarea pe culori
    *   verifica ce user avea culoarea (mock them) si impinge datele cu userId
    *
    *   TODO: impinge rgb image, depth image si skeleton(json) cu user id
    */

}
