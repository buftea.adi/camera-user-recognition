package com.vinci.cameraprocessor.cameraproecssor.skeleton;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Joint implements Serializable {
    private Double confidence;
    @JsonProperty("real.x")
    private Double realX;
    @JsonProperty("real.y")
    private Double realY;
    @JsonProperty("real.z")
    private Double realZ;
    @JsonProperty("proj.x")
    private Double projectionX;
    @JsonProperty("proj.y")
    private Double projectionY;
}
