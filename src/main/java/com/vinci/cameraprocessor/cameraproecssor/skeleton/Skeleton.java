package com.vinci.cameraprocessor.cameraproecssor.skeleton;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Skeleton implements Serializable {
    @JsonProperty("skeleton")
    private Long index;
    @JsonProperty("joint_head")
    private Joint head;
    @JsonProperty("joint_neck")
    private Joint neck;
    @JsonProperty("joint_torso")
    private Joint torso;
    @JsonProperty("joint_waist")
    private Joint waist;

    @JsonIgnore
    private Map<BodyPart, Joint> left = new HashMap<>();

    @JsonIgnore
    private Map<BodyPart, Joint> right = new HashMap<>();

    @JsonAnySetter
    public void setBodyPart(String key, Joint value) {
        Map<BodyPart, Joint> side = key.contains("left") ? left :
                key.contains("right") ? right : null;
        if (side == null) return;
        addJoint(side, key, value);
    }

    private void addJoint(Map<BodyPart, Joint> side, String jsonKey, Joint joint) {
        String key = Arrays.stream(jsonKey.split("_"))
                .reduce((first, second) -> second)
                .orElse(null);
        side.put(BodyPart.getInstance(key), joint);

    }

    @JsonAnyGetter
    public Map<String, Object> getRightForJson() {
        Map<String, Object> result = new HashMap<>();
        right.forEach((bodyPart, joint) -> result.put("joint_right_" + bodyPart.getStringValue(), joint));
        left.forEach((bodyPart, joint) -> result.put("joint_left_" + bodyPart.getStringValue(), joint));
        return result;
    }

}
