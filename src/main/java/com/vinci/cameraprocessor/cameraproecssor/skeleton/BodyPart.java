package com.vinci.cameraprocessor.cameraproecssor.skeleton;

import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum BodyPart {
    COLLAR("collar"),
    SHOULDER("shoulder"),
    HIP("hip"),
    ELBOW("elbow"),
    WRIST("wrist"),
    HAND("hand"),
    KNEE("knee"),
    ANKLE("ankle"),
    UNKNOWN("unknown");

    @Getter
    private String stringValue;

    private static Map<String, BodyPart> parts;
    static  {
        parts = new HashMap<>();
        Arrays.asList(values()).forEach(part ->
                parts.put(part.getStringValue(), part));
    }

    BodyPart(String part) {
        this.stringValue = part;
    }

    public static BodyPart getInstance(String key) {
        return parts.getOrDefault(key, UNKNOWN);
    }
}
