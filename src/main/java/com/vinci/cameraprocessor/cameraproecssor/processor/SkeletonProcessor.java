package com.vinci.cameraprocessor.cameraproecssor.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.BodyPart;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Joint;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Skeleton;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.SkeletonFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

@Slf4j
@Component
public class SkeletonProcessor {

    public boolean shouldProcess(SkeletonFile file) {
        return file.getNumber() != null &&
                file.getNumber() > 0 &&
                !file.getNumber().equals(file.getLastNumber());

    }

    public Optional<SkeletonFile> readFile(String filePath) {
        try {
            SkeletonFile file = new ObjectMapper().readValue(
                    Files.readAllBytes(Paths.get(filePath)),
                    SkeletonFile.class);
            log.debug("File is: {}", file);
            return Optional.of(file);
        } catch (Exception e) {
            log.error("Could not read Skeleton from file: {}", filePath, e);
        }
        return Optional.empty();
    }

    public Joint getBraceletJoint(Skeleton skeleton) {
        return skeleton.getRight().get(BodyPart.WRIST);
    }
}
