package com.vinci.cameraprocessor.cameraproecssor.processor;

import com.vinci.cameraprocessor.cameraproecssor.classification.CommandType;
import com.vinci.cameraprocessor.cameraproecssor.classification.MoveFileAction;
import com.vinci.cameraprocessor.cameraproecssor.handler.AbstractHandler;
import com.vinci.cameraprocessor.cameraproecssor.processor.exception.DrawingFailureException;
import com.vinci.cameraprocessor.cameraproecssor.processor.util.FilesUtils;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Skeleton;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.SkeletonFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Optional;

@Slf4j
@Service
public class ClassificationProcessor {

    @Autowired
    private DrawingProcessor drawingProcessor;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    public void classify(String filePath, SkeletonFile skeletonFile) {
        String timestamp = AbstractHandler.getTimestamp(filePath);
        InputStream depthInputStream = FilesUtils.getDepthInputStream(timestamp);
        Optional<BufferedImage> depthImage = FilesUtils.readImage(depthInputStream);

        if (!depthImage.isPresent()) {
            log.error("Depth image missing for timestamp: {}", timestamp);
            return;
        }

        skeletonFile.getSkeletons().forEach(skeleton -> {
            try {
                BufferedImage drawnImage = drawingProcessor.getImageWithSkeleton(skeleton, depthImage.get());
                displayAndClassify(drawnImage, FilesUtils.getDepthFileName(timestamp), skeleton);
            } catch (DrawingFailureException e) {
                log.error(e.getMessage());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        FilesUtils.closeInputStream(depthInputStream);
        FilesUtils.moveToProcessed(filePath);

    }

    private void displayAndClassify(BufferedImage drawnImage, String depthFilePath, Skeleton skeleton) throws InterruptedException {
        /*  displayImageWithOptions image
         *  wait for input from keyboard: left, right, down, up
         *  based on input: STANDING, SITTING, WALKING(optional)
         *  save in specific folder */
        displayImageWithOptions(drawnImage, depthFilePath, skeleton);
    }

    private void displayImageWithOptions(BufferedImage drawnImage, String depthFilePath, Skeleton skeleton) {
        JDialog frame = new JDialog();
        JPanel panel = new JPanel();

        ImageIcon icon = new ImageIcon(drawnImage);
        JLabel label = new JLabel(icon);

        panel.add(label);

        /*Buttons have action commands that translate into classification actions*/
        Arrays.asList(CommandType.values()).forEach(commandType ->
                addButton(frame, panel, commandType, depthFilePath, skeleton));

        frame.add(panel);

        frame.setSize(new Dimension(drawnImage.getWidth() + 100,drawnImage.getHeight() + 200));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void addButton(JDialog dialog, JPanel panel, CommandType commandType, String depthFilePath, Skeleton skeleton) {

        JButton button = new JButton(commandType.getAction());
        button.setActionCommand(commandType.getAction());

        button.addActionListener(ae -> {
            CommandType command = CommandType.get(ae.getActionCommand());
            log.info("Pressed: {}", command);
            eventPublisher.publishEvent(MoveFileAction.builder()
                    .destination(command)
                    .skeleton(skeleton)
                    .depthImagePath(depthFilePath)
                    .build());
            dialog.dispose();
        });

        panel.add(button);
    }

}
