package com.vinci.cameraprocessor.cameraproecssor.processor;

import com.vinci.cameraprocessor.cameraproecssor.processor.exception.DrawingFailureException;
import com.vinci.cameraprocessor.cameraproecssor.processor.util.FilesUtils;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.BodyPart;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Joint;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Skeleton;
import io.vavr.Function2;
import io.vavr.Function5;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;

@Slf4j
@Service
public class DrawingProcessor {
    private Function5<Graphics2D, Integer, Integer, Joint, Joint, Void> drawOnImage = this::drawOnImage;

    private int index = 0;

    public BufferedImage getImageWithSkeleton(Skeleton skeleton, BufferedImage image) {
        BufferedImage imageWithSkeleton = FilesUtils.deepCopy(image);
        Graphics2D graphics2D = imageWithSkeleton.createGraphics();
        graphics2D.setColor(Color.YELLOW);
        graphics2D.setStroke(new BasicStroke(2f));
        Function2<Joint, Joint, Void> drawLine = drawOnImage.apply(graphics2D,
                imageWithSkeleton.getHeight(), imageWithSkeleton.getWidth());

        /*Draw main bones*/
        drawLine.apply(skeleton.getHead(), skeleton.getNeck());
        drawLine.apply(skeleton.getNeck(), getCollar(skeleton));
        drawLine.apply(getCollar(skeleton), skeleton.getTorso());
        drawLine.apply(skeleton.getTorso(), skeleton.getWaist());
        /*Draw right bones*/
        drawLine.apply(skeleton.getWaist(), skeleton.getRight().get(BodyPart.HIP));
        drawLine.apply(getCollar(skeleton), skeleton.getRight().get(BodyPart.SHOULDER));
        drawSide(drawLine, skeleton.getRight());
        /*Draw left bones*/
        drawLine.apply(getCollar(skeleton), skeleton.getLeft().get(BodyPart.SHOULDER));
        drawLine.apply(skeleton.getWaist(), skeleton.getLeft().get(BodyPart.HIP));
        drawSide(drawLine, skeleton.getLeft());

        return imageWithSkeleton;
    }

    private void drawSide(Function2<Joint, Joint, Void> drawLine, Map<BodyPart, Joint> side) {
        /*Upper side*/
        drawLine.apply(side.get(BodyPart.SHOULDER), side.get(BodyPart.ELBOW));
        drawLine.apply(side.get(BodyPart.ELBOW), side.get(BodyPart.WRIST));
        drawLine.apply(side.get(BodyPart.WRIST), side.get(BodyPart.HAND));
        /*Lower side*/
        drawLine.apply(side.get(BodyPart.HIP), side.get(BodyPart.KNEE));
        drawLine.apply(side.get(BodyPart.KNEE), side.get(BodyPart.ANKLE));

    }

    private Void drawOnImage(Graphics2D graphics2D, int height, int width, Joint jointX, Joint jointY) {
        if (jointX == null || jointY == null) {
            throw new DrawingFailureException("Not enough joints to draw skeleton");
        }
        graphics2D.draw(new Line2D.Double(
                abs(jointX.getProjectionX() * width),
                abs(jointX.getProjectionY() * height),
                abs(jointY.getProjectionX() * width),
                abs(jointY.getProjectionY() * height)
        ));
        return null;
    }

    private Joint getCollar(Skeleton skeleton) {
        return skeleton.getRight().getOrDefault(BodyPart.COLLAR, skeleton.getLeft().get(BodyPart.COLLAR));
    }

    private double abs(double v) {
        return Math.abs(v);
    }

}
