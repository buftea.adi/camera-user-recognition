package com.vinci.cameraprocessor.cameraproecssor.processor.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.SkeletonFile;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.vinci.cameraprocessor.cameraproecssor.Constants.DEPTH_FILE_NAME_FORMAT;
import static com.vinci.cameraprocessor.cameraproecssor.Constants.RGB_FILE_NAME_FORMAT;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FilesUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static InputStream getRgbInputStream(String deviceId, String timestamp) {
        return getInputStream(String.format(RGB_FILE_NAME_FORMAT, timestamp));
    }

    public static InputStream getDepthInputStream(String timestamp) {
        return getInputStream(getDepthFileName(timestamp));
    }

    public static String getDepthFileName(String timestamp) {
        return String.format(DEPTH_FILE_NAME_FORMAT, timestamp);
    }

    private static InputStream getInputStream(String filename) {
        File initialFile = new File(filename);
        try {
            return FileUtils.openInputStream(initialFile);
        } catch (IOException e) {
            log.error("Could not open file {}", filename);
        }
        return null;
    }

    public static Optional<BufferedImage> readImage(InputStream inputStream) {
        if (inputStream != null) {
            try {
                return Optional.ofNullable(ImageIO.read(inputStream));
            } catch (IOException e) {
                log.error("Could not read file for input stream.", e);
            }
        }
        log.error("File not found.");
        return Optional.empty();
    }

    public static BufferedImage deepCopy(BufferedImage image) {
        ColorModel cm = image.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = image.copyData(image.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public static void moveTo(String source, String destination) {
        try {
            Files.move(
                    Paths.get(source),
                    Paths.get(destination));
            log.info("Moved file to processed directory: {}", destination);
        } catch (IOException e) {
            log.error("Failed to move file to processed directory: {}", source, e);
        }
    }

    public static void moveToProcessed(String filePath) {
        String newFilePath = processedFilePath(filePath);
        moveTo(filePath, newFilePath);
    }

    private static String processedFilePath(String filePath) {
        return filePath.replace("skeleton", "processed");
    }

    public static void closeInputStream(InputStream rgbInputStream) {
        try {
            rgbInputStream.close();
        } catch (IOException e) {
            log.error("Could not close input stream for file", e);
        }
    }

    public static void createDirectory(String directoryPath) {
        final Path processedDir = Paths.get(directoryPath);
        if (Files.exists(processedDir)) {
            log.info("Directory already exists: {}", processedDir);
            return;
        }
        try {
            Files.createDirectories(processedDir);
        } catch (IOException e) {
            log.error("Failed to create directory: {}", processedDir);
        }
        log.info("Created directory: {}", processedDir);
    }

    public static void saveToFile(SkeletonFile skeletonFile, String skeletonPath) {
        try {
            FileWriter fileWriter = new FileWriter(skeletonPath);
            fileWriter.write(objectMapper.writeValueAsString(skeletonFile));
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            log.error("Could not save skeletonFile {} to file {} ", skeletonFile, skeletonPath, e);
        }
    }

    public static void copyFile(String source, String destination) {
        try {
            Files.copy(new File(source).toPath(), new File(destination).toPath());
        } catch (IOException e) {
            log.error("Could not copy file from {} to {}", source, destination, e);
        }
    }

    public static List<String> getFileNames(String directoryName) {
        return Arrays.asList(Objects.requireNonNull(new File(directoryName).list()));
    }

    public static boolean isNotDirectory(String s) {
        return !(new File(s).isDirectory());
    }
}
