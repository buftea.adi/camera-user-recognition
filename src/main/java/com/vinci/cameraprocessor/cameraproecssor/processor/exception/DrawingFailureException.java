package com.vinci.cameraprocessor.cameraproecssor.processor.exception;

public class DrawingFailureException extends RuntimeException {
    public DrawingFailureException(String s) {
        super(s);
    }
}
