package com.vinci.cameraprocessor.cameraproecssor.processor;

import com.vinci.cameraprocessor.cameraproecssor.processor.util.FilesUtils;
import com.vinci.cameraprocessor.cameraproecssor.processor.util.colors.ColorThief;
import com.vinci.cameraprocessor.cameraproecssor.skeleton.Joint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class BraceletColorProcessor {

    /*TODO: Hardcoded for now. We should check all colors registered to users of device id*/
    private static List<String> watchedColors = Arrays.asList("red", "green", "blue");

    public Optional<String> findBraceletColor(BufferedImage rgbImage, Joint braceletJoint) {
        if (braceletJoint == null || braceletJoint.getConfidence() < 0.5) {
            return Optional.empty();
        }
        BufferedImage braceletImage = FilesUtils.deepCopy(rgbImage.getSubimage(
                (int) Math.round(braceletJoint.getProjectionX() * rgbImage.getWidth()) - 5,
                (int) Math.round(braceletJoint.getProjectionY() * rgbImage.getHeight()) - 5,
                20,
                30));

        saveTmp(braceletImage); /*TODO: delete this line*/

        return Optional.ofNullable(getMajorityColor(braceletImage));
    }

    private String getMajorityColor(BufferedImage braceletImage) {
        int[][] dominantColors = ColorThief.getPalette(braceletImage, 5, 10, true);
        if (dominantColors == null) {
            return null;
        }
        List<String> colors = Arrays.stream(dominantColors)
                .map(this::toColorString)
                .filter(Objects::nonNull)
                .filter(watchedColors::contains)
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(colors)) {
            return null;
        }
        /*Returns the one with higher occurrence*/
        return colors.get(0);
    }

    private String toColorString(int[] color) {
        float[] hsb = new float[3];
        Color.RGBtoHSB(color[0], color[1], color[2], hsb);
        float deg = hsb[0]*360;
        String colorString = null;
        if      (deg >=   0 && deg <  30) colorString = "red";
        else if (deg >=  90 && deg < 150) colorString = "green";
        else if (deg >= 210 && deg < 270) colorString ="blue";
        else if (deg >= 330) colorString ="red";
        return colorString;
    }

    @Deprecated/*TODO: delete this method*/
    private void saveTmp(BufferedImage braceletImage) {
        File outputfile = new File("tmpBraceletImage.png");
        try {
            ImageIO.write(braceletImage, "png", outputfile);
            log.info("Wrote tmp bracelet image");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
