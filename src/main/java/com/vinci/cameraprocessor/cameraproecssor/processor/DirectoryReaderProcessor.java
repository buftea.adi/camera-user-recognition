package com.vinci.cameraprocessor.cameraproecssor.processor;

import com.vinci.cameraprocessor.cameraproecssor.handler.ClassifySkeletonHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.vinci.cameraprocessor.cameraproecssor.Constants.SKELETON_DIR_VALUE;

@Slf4j
@Service
public class DirectoryReaderProcessor {

    @Autowired
    private ClassifySkeletonHandler classifySkeletonHandler;

//    @Scheduled(fixedDelay = Long.MAX_VALUE)
    public void processFiles() {
        File skeletonsDir = new File(SKELETON_DIR_VALUE);
        List<String> files = Stream.of(skeletonsDir.list())
                .map(name -> SKELETON_DIR_VALUE + "/" + name)
                .collect(Collectors.toList());
        files.forEach(classifySkeletonHandler::handle);
    }
}
