package com.vinci.cameraprocessor.cameraproecssor;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    /*TODO: correct base dir path (must contain "/" suffix)*/
    private static final String BASE_PATH = "";

    public static final String SKELETON_DIR_VALUE = BASE_PATH + "skeleton";

    public static final String PROCESSED_DIR_VALUE = BASE_PATH + "processed";

    /*TODO correct file paths/names*/
    public static final String RGB_FILE_NAME_FORMAT = BASE_PATH + "rgb/TEST_APP_CAMERA_%srgb.png";
    public static final String DEPTH_FILE_NAME_FORMAT = BASE_PATH + "depth/TEST_APP_CAMERA_%sdepth.png";
    /*TODO: switch to format with deviceId when app in camera is adapted*/
//    private static final String RGB_FILE_NAME_FORMAT = BASE_PATH + "rgb/%s_%srgb.png";


    public static final String DATASET_BASE_PATH = BASE_PATH + "dataset";
    public static final String IMAGE_EXTENSION = ".png";
    public static final String JSON_EXTENSION = ".json";
}
