package com.vinci.cameraprocessor.cameraproecssor.listener;

import com.vinci.cameraprocessor.cameraproecssor.handler.DirectoryEventsHandler;
import com.vinci.cameraprocessor.cameraproecssor.handler.ClassifySkeletonHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.vinci.cameraprocessor.cameraproecssor.Constants.SKELETON_DIR_VALUE;

@Slf4j
@Component
public class DirectoryListener {
    private List<String> consumed = new ArrayList<>();

    @Autowired
    private DirectoryEventsHandler directoryEventsHandler;

    @Autowired
    private ClassifySkeletonHandler classifySkeletonHandler;

//    @Scheduled(fixedDelay = Long.MAX_VALUE)
    public void setUpWatcher() {
        try {
            ExecutorService threadPool = Executors.newCachedThreadPool();
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(SKELETON_DIR_VALUE);

            path.register( watchService, StandardWatchEventKinds.ENTRY_MODIFY);

            WatchKey key;
            while ((key = watchService.take()) != null) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    if (consumed.contains(event.context().toString())) {
                        return;
                    }
                    consumed.add(event.context().toString());

                    log.info("Event kind: {}. File affected: {}", event.kind(), event.context());
                    String filePath = SKELETON_DIR_VALUE + "/" +((Path)event.context()).getFileName().toString();
                    Thread.sleep(100);
                    threadPool.submit(() -> classifySkeletonHandler.handle(filePath));
                }
                key.reset();
            }
            log.info("Done");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
